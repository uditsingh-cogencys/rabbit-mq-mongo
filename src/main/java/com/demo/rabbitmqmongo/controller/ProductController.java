package com.demo.rabbitmqmongo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.rabbitmqmongo.dao.ProductDao;
import com.demo.rabbitmqmongo.entity.Product;
import com.demo.rabbitmqmongo.service.RabbitMQSender;

@RestController
@RequestMapping(value = "/rabbitmq")
public class ProductController {
	
	@Autowired
	RabbitMQSender rabbitMQSender;
	
	@Autowired
	public ProductDao productDao;
	
	@PostMapping(value = "/product")
	public Product createProduct(@RequestBody Product prod) {
		
		return productDao.insert(prod);
	}
	
	// for mongo db data fetch 
	
	/*@GetMapping(value = "/product")
	public List<Product> getProduct() {
	    return productDao.findAll();

	}*/
	
	@GetMapping(value = "/product")
	public String producer(@RequestParam("productName") String productName,@RequestParam("productId") 
	                        int productId, @RequestParam("productCost") String productCost) {
	
	Product prod=new Product();
	prod.setProductId(productId);
	prod.setProductName(productName);
	prod.setProductCost(productCost);
		rabbitMQSender.send(prod);

		return "Message sent to the RabbitMQ Server Successfully";
	}
	
}
