package com.demo.rabbitmqmongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitMqMongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RabbitMqMongoApplication.class, args);
	}

}
