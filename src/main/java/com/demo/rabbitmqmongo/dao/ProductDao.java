package com.demo.rabbitmqmongo.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.demo.rabbitmqmongo.entity.Product;

public interface ProductDao extends MongoRepository<Product, String> {

}
